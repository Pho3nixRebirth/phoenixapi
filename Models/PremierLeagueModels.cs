﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixAPI.Models
{
    public class TableModel
    {
        public string PlayerName { get; set; }
        public List<string> Table { get; set; }
        public int _id { get; set; }
    }

    public class TableRootObject
    {
        [JsonProperty("standings")]
        public List<StandingItems> Results { get; set; }

        [JsonProperty("season")]
        public SeasonItem Matchday { get; set; }
    }

    public class StandingItems
    {
        [JsonProperty("table")]
        public List<TeamItem> standing;
        public string type;
    }

    public class SeasonItem
    {
        public string id;
        public string startDate;
        public string endDate;
        public int currentMatchday;
    }

    public class TeamItem
    {
        [JsonProperty("position")]
        public int position;
        [JsonProperty("team")]
        public TeamInfo team;
        [JsonProperty("playedGames")]
        public int playedGames;
        [JsonProperty("points")]
        public int points;
    }

    public class TeamInfo
    {
        [JsonProperty("id")]
        public int id;
        [JsonProperty("name")]
        public string name;
    }

    public class GameweekTable
    {
        public int Gameweek { set; get; }
        public List<TableObject> Table { get; set; }
        public int _id { get; set; }
    }

    public class TableObject
    {
        public string TeamName { get; set; }
        public int Position { get; set; }
        public int PlayedMatches { get; set; }
        public string Points { get; set; }
    }


    public class FantasyRootObjects
    {
        [JsonProperty("standings")]
        public FantasyStandingObject standings;
    }

    public class FantasyStandingObject
    {
        [JsonProperty("results")]
        public List<FantasyResultObject> results;
    }

    public class FantasyResultObject
    {
        [JsonProperty("entry_name")]
        public string entry_name;
        [JsonProperty("player_name")]
        public string player_name;
        [JsonProperty("rank")]
        public int rank;
        [JsonProperty("last_rank")]
        public int last_rank;
        [JsonProperty("movement")]
        public string movement;
        [JsonProperty("total")]
        public int total;
        [JsonProperty("event_total")]
        public int event_total;
    }

    public class FantasyPlayerDatabaseRootObject
    {
        [JsonProperty("elements")]
        public List<FantasyPlayerObject> elements;
    }

    public class FantasyPlayerObject
    {
        [JsonProperty("id")]
        public int id;

        [JsonProperty("first_name")]
        public string first_name;

        [JsonProperty("second_name")]
        public string second_name;

        [JsonProperty("total_points")]
        public int total_points;

        [JsonProperty("web_name")]
        public string web_name;

        [JsonProperty("element_type")]
        public int element_type;
    }
}
