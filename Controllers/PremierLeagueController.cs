﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using LiteDB;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PhoenixAPI.Models;

namespace PhoenixAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PremierLeagueController : ControllerBase
    {
        [HttpGet("GetPlayerTables")]
        public IEnumerable<TableModel> GetPlayerTables()
        {
            LiteDatabase db = new LiteDatabase(@"PremierLeagueData.db");
            LiteCollection<TableModel> Tables = db.GetCollection<TableModel>("PremierLeagueCollection");

            var tableList = Tables.FindAll().ToList();
            db.Dispose();

            //Fulhack
            tableList.Where(x => x.PlayerName == "Tony").FirstOrDefault().PlayerName = "Anthonio";

            return tableList;
        }

        // GET api/GetPlayerTables
        [HttpGet("GetCurrentPremierLeagueTable")]
        public GameweekTable GetCurrentPremierLeagueTable()
        {
            List<TableObject> positionToday = new List<TableObject>();
            int gameweek = 1;

            //Data hämtas från: http://www.football-data.org/code_samples
            //string url = "http://api.football-data.org/v1/competitions/445/leagueTable";
            string url = "http://api.football-data.org/v2/competitions/PL/standings";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/json";
            request.Headers["X-Auth-Token"] = "8fa1fbe33f4e4a69a7d9d6b9b25db860";

            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(responseStream))
                    {
                        var jsonResponse = sr.ReadToEnd();
                        var list = JsonConvert.DeserializeObject<TableRootObject>(jsonResponse);
                        StandingItems tempItem = list.Results[0];
                        gameweek = list.Matchday.currentMatchday;

                        foreach (TeamItem teamItem in tempItem.standing)
                        {
                            string teamName = teamItem.team.name;
                            teamName = teamName.Replace(" FC", "");
                            teamName = teamName.Replace("AFC ", "");
                            teamName = teamName.Replace(" Town", "");

                            TableObject obj = new TableObject
                            {
                                TeamName = teamName,
                                Position = teamItem.position,
                                PlayedMatches = teamItem.playedGames,
                                Points = teamItem.points.ToString() + "p"
                            };

                            positionToday.Add(obj);
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                throw ex;
            }

            GameweekTable currentTable = new GameweekTable
            {
                Gameweek = gameweek,
                Table = positionToday
            };

            return currentTable;
        }

        [HttpPost("SaveCurrentGameweekTable")]
        public List<GameweekTable> SaveCurrentGameweekTable()
        {
            GameweekTable premierLeagueStanding = GetCurrentPremierLeagueTable();
            LiteDatabase db = new LiteDatabase(@"PremierLeagueData.db");
            LiteCollection<GameweekTable> Tables = db.GetCollection<GameweekTable>("PremierLeagueGameweekTableCollection");

            if (Tables.Find(x => x.Gameweek == premierLeagueStanding.Gameweek - 1).Count() == 0)
            {
                premierLeagueStanding.Gameweek -= 1;

                if (premierLeagueStanding.Table[0].TeamName != "")
                {
                    Tables.Insert(premierLeagueStanding);
                }
            }

            var tableList = Tables.FindAll().ToList();
            db.Dispose();

            return tableList;
        }

        [HttpGet("GetAllGameweekTables")]
        public List<GameweekTable> GetAllGameweekTables()
        {
            LiteDatabase db = new LiteDatabase(@"PremierLeagueData.db");
            LiteCollection<GameweekTable> Tables = db.GetCollection<GameweekTable>("PremierLeagueGameweekTableCollection");

            var tableList = Tables.FindAll().ToList();
            db.Dispose();

            return tableList;
        }

        [HttpGet("CalculatePlayerPoints")]
        public List<Tuple<string, int>> CalculatePlayerPoints()
        {
            IEnumerable<TableModel> playerTables = GetPlayerTables();
            GameweekTable premierLeagueStanding = GetCurrentPremierLeagueTable();
            List<Tuple<string, int>> results = new List<Tuple<string, int>>();

            foreach (TableModel item in playerTables)
            {
                List<string> table = item.Table;
                int points = 0;

                for (int i = 0; i < table.Count; i++)
                {
                    if (table[i] == premierLeagueStanding.Table[i].TeamName)
                    {
                        points += 3;
                    }
                    else
                    {
                        if (i > 0)
                        {
                            if (table[i] == premierLeagueStanding.Table[i - 1].TeamName)
                            {
                                points += 1;
                            }
                        }

                        if (i < table.Count - 1)
                        {
                            if (table[i] == premierLeagueStanding.Table[i + 1].TeamName)
                            {
                                points += 1;
                            }
                        }
                    }
                }

                results.Add(new Tuple<string, int>(item.PlayerName, points));
            }

            return results;
        }

        [HttpGet("GetPremierLeagueFantasyStanding")]
        public async Task<FantasyRootObjects> GetPremierLeagueFantasyStanding()
        {
            Uri uri = new Uri("https://fantasyfunction.azurewebsites.net/api/GetFantasyTableStanding");

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = await client.GetAsync(uri))
                {
                    using (Stream responseStream = await response.Content.ReadAsStreamAsync())
                    {
                        using (StreamReader sr = new StreamReader(responseStream))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            FantasyRootObjects list = JsonConvert.DeserializeObject<FantasyRootObjects>(jsonResponse);

                            return list;
                        }
                    }
                }
            }
        }

        [HttpGet("GetCementEleven")]
        public async Task<List<Tuple<string, List<FantasyPlayerObject>>>> GetCementEleven()
        {
            List<Tuple<string, List<FantasyPlayerObject>>> CementStanding = new List<Tuple<string, List<FantasyPlayerObject>>>();
            Uri uri = new Uri("https://fantasy.premierleague.com/api/bootstrap-static/");
            int[] StefanPlayers = new int[11] { 93, 141, 205, 503, 134, 151, 265, 133, 143, 410, 409 }; //Stefan: 93,141,205,503,134,151,265,133,143,410,409
            int[] EdwardPlayers = new int[11] { 47, 42, 405, 64, 214, 218, 414, 465, 68, 409, 278 }; //Edward: 47,42,405,64,214,218,414,465,68,450,278
            int[] AndréPlayers = new int[11] { 93, 467, 337, 183, 458, 75, 218, 265, 414, 11, 410 }; //André: 93,467,337,183,458,75,218,265,414,11,410

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = await client.GetAsync(uri))
                {
                    using (Stream responseStream = await response.Content.ReadAsStreamAsync())
                    {
                        using (StreamReader sr = new StreamReader(responseStream))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            FantasyPlayerDatabaseRootObject list = JsonConvert.DeserializeObject<FantasyPlayerDatabaseRootObject>(jsonResponse);

                            var stefanQuery = list.elements.Where(item => StefanPlayers.Contains(item.id)).OrderBy(x => x.element_type).ToList();
                            var edwardQuery = list.elements.Where(item => EdwardPlayers.Contains(item.id)).OrderBy(x => x.element_type).ToList();
                            var andreQuery = list.elements.Where(item => AndréPlayers.Contains(item.id)).OrderBy(x => x.element_type).ToList();

                            Tuple<string, List<FantasyPlayerObject>> stefanTuple = new Tuple<string, List<FantasyPlayerObject>>("Stefan", stefanQuery);
                            Tuple<string, List<FantasyPlayerObject>> edwardTuple = new Tuple<string, List<FantasyPlayerObject>>("Edward", edwardQuery);
                            Tuple<string, List<FantasyPlayerObject>> andreTuple = new Tuple<string, List<FantasyPlayerObject>>("André", andreQuery);

                            CementStanding.Add(stefanTuple);
                            CementStanding.Add(edwardTuple);
                            CementStanding.Add(andreTuple);

                            return CementStanding;
                        }
                    }
                }
            }
        }
    }
}